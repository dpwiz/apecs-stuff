module TheGame (run, thePlay, stage) where

import Prelude

import Apecs
import Control.Lens ((^.))
import Graphics.Gloss.Interface.IO.Interact (Event(..))
import Linear.V2 (_x, _y)

import qualified  Graphics.Gloss as G
import qualified Apecs.Physics as P
import qualified Apecs.Physics.Gloss as APG

import Play.Class (Play(..), Stage, defaultStage, mkPlay, runPlay)
import System.Animate (stepAnimation) -- , dumpAnimation)
import World.Types (World, initWorld)
import Component.Animation (Animation(..))

import qualified Component.Animation as Animation
import qualified Component.Draw as Draw

stage :: Stage
stage = defaultStage

run :: IO ()
run = runPlay thePlay defaultStage

thePlay :: Play World
thePlay = (mkPlay initWorld)
  { playSetup = setup
  , playDraw = draw
  , playStepEvent = stepEvent
  , playStepTime = stepTime
  }

setup :: System World ()
setup = do
  set global
    ( APG.Camera 0 50
    , P.earthGravity
    )

  spawnThingy

spawnThingy :: System World ()
spawnThingy = do
  let someShape = P.cRectangle 1
  -- let somePicture = G.color G.red $ APG.convexToPicture someShape
  someBody <- newEntity
    ( P.DynamicBody
    , P.Density 1
    , P.BodyMass 1
    , P.Moment 1
    , P.Elasticity 0.9
    , P.Velocity (P.V2 4 10)
    , P.Torque 300
    , Animation.bounce (0.0, 0.5)
    -- , Draw.Frames [somePicture]
    )
  liftIO $ print someBody
  ss <- newEntity $ P.Shape someBody someShape
  liftIO $ print ss


draw :: System World G.Picture
draw = cfoldM (\pic -> fmap (mappend pic) . fn) mempty
  where
    fn = APG.drawBody

  -- APG.worldTransform transform <$> foldM foldfn mempty shapes
  -- pure mempty
-- cfold render mempty
--   where
--     render pic (Animation{current}, P.Position pos, P.Angle ang, P.BodyPicture) =
--       mappend pic
--         . G.translate (realToFrac $ pos ^. _x) (realToFrac $ pos ^. _y)
--         . G.rotate (realToFrac ang)
--         $ G.color G.white $
--             G.circle (current * 100)

stepEvent :: Event -> System World ()
stepEvent event = do
  liftIO $ print event
  case event of
    EventKey btn APG.Down _mods _pos ->
      case btn of
        APG.SpecialKey APG.KeyEsc ->
          exit
        APG.SpecialKey APG.KeySpace ->
          pure ()
        _ ->
          spawnThingy
    EventMotion _pos ->
      spawnThingy
    _ ->
      pure ()
  where
    exit = fail "halt"

stepTime :: Float -> System World ()
stepTime dt = do
  stepAnimation dt
  P.stepPhysics (realToFrac dt)
  -- dumpAnimation
