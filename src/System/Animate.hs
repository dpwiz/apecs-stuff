module System.Animate
  ( stepAnimation
  , dumpAnimation
  ) where

import Prelude

import Apecs
import Control.Monad.IO.Class (MonadIO(..))

import Component.Animation (Animation(..))
import qualified Component.Animation as Animation

stepAnimation
  :: forall w m
  . ( Get w m Animation
    , Set w m Animation
    , Members w m Animation
    )
  => Float -> SystemT w m ()
stepAnimation = cmap . Animation.step

dumpAnimation :: (Has w m Animation, MonadIO m) => SystemT w m ()
dumpAnimation = cfoldM_ dump ()
  where
    dump () (a :: Animation) =
      liftIO (print a)
