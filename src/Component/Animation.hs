module Component.Animation
  ( Animation(..)
  , Loop(..)
  , Direction(..)
  , still
  , once
  , cycle
  , bounce
  , step
  ) where

import Prelude hiding (cycle)

import Apecs

data Animation = Animation
  { range :: (Float, Float)
  , current :: Float
  , loop :: Loop
  , direction :: Direction
  } deriving (Show)

data Loop
  = Still
  | Once
  | Cycle
  | Bounce
  deriving (Eq, Ord, Enum, Bounded, Show)

data Direction
  = Forward
  | Reverse
  deriving (Eq, Ord, Enum, Bounded, Show)

instance Component Animation where
  type Storage Animation = Map Animation

still :: (Float, Float) -> Animation
still r = Animation
  { range = r
  , current = fst r
  , loop = Still
  , direction = Forward
  }

once :: (Float, Float) -> Animation
once r = Animation
  { range = r
  , current = fst r
  , loop = Once
  , direction = Forward
  }

cycle :: (Float, Float) -> Animation
cycle r = Animation
  { range = r
  , current = fst r
  , loop = Cycle
  , direction = Forward
  }

bounce :: (Float, Float) -> Animation
bounce r = Animation
  { range = r
  , current = fst r
  , loop = Bounce
  , direction = Forward
  }

step :: Float -> Animation -> Animation
step dt a =
  case direction a of
    Forward ->
      stepForward dt a
    Reverse ->
      stepReverse dt a

stepForward  :: Float -> Animation -> Animation
stepForward dt a =
  case loop a of
    -- Do nothing
    Still ->
      a
    -- Clamp and hold
    Once ->
      if overEnd then
        a
          { current = rEnd
          , loop = Still
          }
      else
        nextA
    -- Overflow and restart
    Cycle ->
        if overEnd then
          a { current = next - rEnd + rStart }
        else
          nextA
    Bounce ->
      if overEnd then
        a
          { current = rEnd * 2 - next
          , direction = Reverse
          }
      else
        nextA
  where
    (rStart, rEnd) = range a
    overEnd = next > rEnd
    next = current a + dt
    nextA = a { current = next }

stepReverse :: Float -> Animation -> Animation
stepReverse dt a =
  case loop a of
    -- Do nothing
    Still ->
      a
    -- Clamp and hold
    Once ->
      if overStart then
        a
          { current = rEnd
          , loop = Still
          }
      else
        nextA
    -- Overflow and restart
    Cycle ->
        if overStart then
          a { current = rEnd - next + rStart }
        else
          nextA
    Bounce ->
      if overStart then
        a
          { current = rStart * 2 - next
          , direction = Forward
          }
      else
        nextA
  where
    (rStart, rEnd) = range a
    overStart = next < rStart
    next = current a - dt
    nextA = a { current = next }
