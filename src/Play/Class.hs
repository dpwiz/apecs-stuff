{-# LANGUAGE AllowAmbiguousTypes #-}

module Play.Class
  ( Play(..)
  , mkPlay
  , Stage(..)
  , defaultStage
  , runPlay
  , playSystem
  ) where

import Prelude

import Apecs (Has, System, runWith)
import qualified Apecs.Gloss as AG

data Play w = Play
  { playInit :: IO w
  , playSetup :: System w ()
  , playDraw :: System w AG.Picture
  , playStepEvent :: AG.Event -> System w ()
  , playStepTime :: Float -> System w ()
  }

data Stage = Stage
  { stageDisplay :: AG.Display
  , stageColor :: AG.Color
  , stageFPS :: Int
  } deriving (Show)

defaultStage :: Stage
defaultStage = Stage
  { stageDisplay = AG.FullScreen
  , stageColor = AG.black
  , stageFPS = 60
  }

mkPlay :: IO w -> Play w
mkPlay initWorld = Play
  { playInit = initWorld
  , playSetup = pure ()
  , playDraw = pure mempty
  , playStepEvent = const $ pure ()
  , playStepTime = const $ pure ()
  }

runPlay :: Has w IO AG.Camera => Play w -> Stage -> IO ()
runPlay play@Play{playInit, playSetup} stage = do
  world0 <- playInit
  runWith world0 $ do
    playSetup
    playSystem play stage

playSystem :: Has w IO AG.Camera => Play w -> Stage -> System w ()
playSystem Play{..} Stage{..} =
  AG.play stageDisplay stageColor stageFPS playDraw playStepEvent playStepTime
