{-# LANGUAGE TemplateHaskell #-}

module World.Types
  ( World
  , initWorld
  ) where

import Prelude

import Apecs
import Apecs.Gloss (Camera)

import qualified Apecs.Physics as AP

import Component.Animation (Animation)
import Component.Draw (Draw)

makeWorld "World"
  [ ''Camera -- for apecs-gloss
  , ''AP.Physics
  , ''Animation
  , ''Draw
  ]
