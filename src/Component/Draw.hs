module Component.Draw where

import Prelude

import Apecs (Component(..), Map)
import Graphics.Gloss (Picture)

newtype Draw = Frames
  { frames :: [Picture]
  } deriving (Show)

instance Component Draw where
  type Storage Draw = Map Draw
