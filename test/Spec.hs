import World.Types (initWorld)

import Prelude

import Apecs

import qualified Component.Animation as Animation
import System.Animate (stepAnimation, dumpAnimation)

main :: IO ()
main = do
  testAnimation

testAnimation :: IO ()
testAnimation = do
  world0 <- initWorld

  runWith world0 $ do
    _still <- newEntity $
      Animation.still (0, 1)

    _once <- newEntity $
      Animation.once (0, 1)

    _cycle <- newEntity $
      Animation.cycle (0, 1)

    _bounce <- newEntity $
      Animation.bounce (0, 1)

    liftIO $ putStrLn "=== t = 0.0 ==="
    stepAnimation 0.0
    dumpAnimation

    liftIO $ putStrLn "=== t = 0.5 ==="
    stepAnimation 0.50
    dumpAnimation

    liftIO $ putStrLn "=== t = 1.0 ==="
    stepAnimation 0.50
    dumpAnimation

    liftIO $ putStrLn "=== t = 1.25 ==="
    stepAnimation 0.25
    dumpAnimation

    liftIO $ putStrLn "=== t = 1.50 ==="
    stepAnimation 0.25
    dumpAnimation

    liftIO $ putStrLn "=== t = 2.50 ==="
    stepAnimation 1.0
    dumpAnimation
