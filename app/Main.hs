module Main where

import Prelude

import TheGame (run)

main :: IO ()
main = run
